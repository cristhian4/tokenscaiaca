const { authJwt } = require('../middleware');
const controller = require('../controllers/user.controller');

module.exports = function (app) {
  app.post(
    '/registerToken',
    /* [authJwt.verifyToken], */ controller.registerToken
  );

  app.post(
    '/redeemTokens',
    /* [authJwt.verifyToken], */ controller.redeemTokens
  );
};
