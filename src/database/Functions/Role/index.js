const { role } = require('../../Models');
const findAll = require('./findAll');
const createRole = require('./createRole');
module.exports = {
  findAll: findAll(role),
  createRole: createRole(role),
};
