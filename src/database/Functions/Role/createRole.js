module.exports = (role) => (name) => {
  return role.create({
    name,
  });
};
