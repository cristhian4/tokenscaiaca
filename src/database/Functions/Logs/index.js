const { logs } = require('../../Models');
const findAll = require('./findAll');
const giveTokens = require('./giveTokens');
const insert = require('./insert');

module.exports = {
  findAll: findAll(logs),
  giveTokens: giveTokens(logs),
  insert: insert(logs),
};
