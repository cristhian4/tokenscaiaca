module.exports = (logs) => (tokenId, userId, createdAt) => {
  return logs.create({
    tokenId,
    userId,
    createdAt,
  });
};
