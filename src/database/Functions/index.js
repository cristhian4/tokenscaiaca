module.exports = {
  Token: require('./Token'),
  User: require('./User'),
  Logs: require('./Logs'),
  Role: require('./Role'),
};
