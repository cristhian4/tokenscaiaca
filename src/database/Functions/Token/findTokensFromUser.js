module.exports = (token) => (userId, RewardPrice) =>
  token.findAll({
    limit: RewardPrice,
    where: { userId },
    raw: true,
  });
