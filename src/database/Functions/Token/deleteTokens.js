module.exports = (token) => (tokenId, userId) => {
  return token.destroy({
    where: {
      tokenId,
      userId,
    },
  });
};
