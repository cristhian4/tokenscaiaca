module.exports = (token) => (userId, tokenId) => {
  return token.create({
    userId,
    tokenId,
  });
};
