const { token } = require('../../Models');
const findAll = require('./findAll');
const findByUserId = require('./findByUserId');
const insert = require('./insert');
const findTokensFromUser = require('./findTokensFromUser');
const deleteTokens = require('./deleteTokens');

module.exports = {
  findAll: findAll(token),
  findByUserId: findByUserId(token),
  insert: insert(token),
  findTokensFromUser: findTokensFromUser(token),
  deleteTokens: deleteTokens(token),
};
