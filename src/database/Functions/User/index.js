const { user } = require('../../Models');
const findAll = require('./findAll');
const findByUserId = require('./findByUserId');
const insert = require('./insert');

module.exports = {
  findAll: findAll(user),
  findByUserId: findByUserId(user),
  insert: insert(user),
};
