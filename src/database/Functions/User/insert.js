module.exports = (user) => (userId) => {
  return user.create({
    userId,
  });
};
