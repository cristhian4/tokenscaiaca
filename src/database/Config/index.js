const Sequelize = require('sequelize');
const { db } = require('./environment');
require('dotenv').config();
let sequelize = null;
module.exports = () => {
  if (sequelize != null) {
    return sequelize;
  }
  if (process.env.NODE_ENV === 'QA') {
    sequelize = new Sequelize(process.env.DATABASE_URL, db);
  } else {
    sequelize = new Sequelize(db);
  }
  return sequelize;
};
