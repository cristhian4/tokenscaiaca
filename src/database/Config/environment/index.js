require('dotenv').config();
const development = require('./development');
const QA = require('./QA');
const { NODE_ENV } = process.env;
let currentenv = development;
if (NODE_ENV === 'QA') {
  currentenv = QA;
}
module.exports = currentenv;
