module.exports = (sequelize, Sequelize) => {
  const token = sequelize.define(
    'token',
    {
      tokenId: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
      },
      userId: {
        type: Sequelize.STRING,
        primaryKey: false,
        allowNull: false,
        autoIncrement: false,
      },
    },
    {
      freezeTableName: true,
    }
  );

  token.associate = (models) => {
    token.belongsTo(models.user, {
      foreignKey: 'userId',
    });
  };

  return token;
};
