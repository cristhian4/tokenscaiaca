module.exports = (sequelize, Sequelize) => {
  const user = sequelize.define(
    'user',
    {
      userId: {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false,
      },
    },
    {
      freezeTableName: true,
      timestamps: false,
    }
  );

  user.associate = (models) => {
    user.hasMany(models.token, {
      foreignKey: 'userId',
    });
  };

  return user;
};
