module.exports = (sequelize, Sequelize) => {
  const logs = sequelize.define(
    'logs',
    {
      tokenId: {
        type: Sequelize.UUID,

        allowNull: false,
      },
      userId: {
        type: Sequelize.STRING,
        primaryKey: false,
        allowNull: false,
        autoIncrement: false,
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        autoIncrement: false,
      },
    },
    {
      freezeTableName: true,
    }
  );

  return logs;
};
