module.exports = (sequelize, datatypes) => {
  const auth = sequelize.define(
    'auth',
    {
      userId: {
        type: datatypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      username: {
        type: datatypes.STRING,
        allowNull: false,
      },
      email: {
        type: datatypes.STRING,
        allowNull: false,
      },
      password: {
        type: datatypes.STRING,
      },
    },
    {
      freezeTableName: true,
      timestamps: false,
    }
  );

  auth.associate = (models) => {
    auth.belongsToMany(models.role, {
      through: 'user_roles',
      foreignKey: 'userId',
      otherKey: 'roleId',
      timestamps: false,
    });

    auth.hasOne(models.refreshToken, {
      foreignKey: 'userId',
      targetKey: 'userId',
    });
  };
  return auth;
};
