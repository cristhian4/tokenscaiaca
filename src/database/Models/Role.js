module.exports = (sequelize, Sequelize) => {
  const role = sequelize.define(
    'role',
    {
      roleId: {
        type: Sequelize.BIGINT,
        primaryKey: true,
      },
      name: {
        type: Sequelize.STRING,
      },
    },
    {
      freezeTableName: true,
      timestamps: false,
    }
  );

  role.associate = (models) => {
    role.belongsToMany(models.auth, {
      through: 'user_roles',
      foreignKey: 'roleId',
      otherKey: 'userId',
      timestamps: false,
    });
  };
  return role;
};
