const db = require('../database/Functions');
const { User, Token, Logs } = db;

const registerToken = async (req, res) => {
  console.log('llegamos a registrar');
  let fID = req.body.usersPostulate;

  fID.forEach(async (user) => {
    const userId = user.userId;

    await User.findByUserId(userId).then(async (result) => {
      if (!result) {
        result = await User.insert(userId);
      }
    });
  });

/*   fID.forEach(async (user) => {
    const userId = user.userId;
    const tokens = user.tokens;

    await User.findByUserId(userId).then(async (result) => {
      if (!result) {
        result = await User.insert(userId);
      }
    });

    for (let i = 0; i < tokens; i++) {
      //console.log(i);
      /*  if (tokens === 0) {
        continue;
      } */
      await Token.insert(userId, res.tokenId);
    }
  }); */

  res.status(200).json('Tokens registrados');
};

const redeemTokens = async (req, res) => {
  const { userId } = req.body;
  const { RewardPrice } = req.body;

  const resToken = await Token.findTokensFromUser(userId, RewardPrice);

  const insertTokenArr = [];
  const deleteTokens = [];

  resToken.forEach((token) => {
    insertTokenArr.push(Logs.insert(token.tokenId, userId, token.createdAt));
  });

  await Promise.all(insertTokenArr);

  resToken.forEach((token) => {
    deleteTokens.push(Token.deleteTokens(token.tokenId, userId));
  });

  await Promise.all(deleteTokens);

  res.status(200).send();
};
module.exports = { registerToken, redeemTokens };
