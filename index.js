const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

var corsOptions = {
  origin: 'http://localhost:8081',
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
/* app.get('/', (req, res) => {
  res.json({ message: 'Welcome to application caiaca Token Cris .' });
}); */

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}. `);
});

app.get('/', (req, res) => {
  const db = require('./src/database/Functions');
  const { Token, User, UserToken } = db;

  Token.findAll().then((result) => {
    res.send(result);
  });

  /*  User.findAll().then((result) => {
    res.send(result);
  }); */

  /* UserToken.findAll().then((result) => {
    res.send(result);
  }); */
});

// routes
require('./src/routes/login.routes')(app);
require('./src/routes/auth.routes')(app);
require('./src/routes/user.routes')(app);
